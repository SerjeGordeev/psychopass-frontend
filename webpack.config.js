
const _ = require("lodash");

let dirAliases = {
  main: __dirname + "/app/main",
  components: __dirname + "/app/components",
  npm: __dirname + "/node_modules",
  images: __dirname + "/app/images",
  fonts: __dirname + "/fonts",
  styles: __dirname + "/app/styles"
};

module.exports = {
  context: __dirname + '/app',
  resolve: {
    root: __dirname,
    alias: _.assign({}, dirAliases)
  },
  entry: './index.js',
  output: {
    path: __dirname + '/app',
    filename: './bundle.js'
  },
  module: {
    loaders: [
      {test: /\.js$/, loader: 'babel'},
      {test: /\.html$/, loader: 'raw'},
      {test: /\.css$/, loader: 'style!css'},
      {test: /\.scss$/, loader: 'style!css!sass'},
      {test: /\.(png|jpg|gif|svg)$/, loader: "url-loader"}
    ]
  },
  devServer: {
    port: 8080,
    contentBase: "./app",
    proxy: {
      "**/api/**": {
        target: 'http://localhost:8780',
        changeOrigin: true,
        secure: false,
        toProxy: true
      }
    },
    historyApiFallback: {
      index: "/"
    }
  }
};
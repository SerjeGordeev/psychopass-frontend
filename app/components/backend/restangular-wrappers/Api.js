angular
	.module("app.backend")
	.factory("Api", Api);

function Api(CookiesStorage, Restangular, Interceptors) {
	return Restangular.withConfig(RestangularConfigurer => {

		const headers = {
			Accept: "application/json"
		};

		if (CookiesStorage.get("authToken")) {
			headers.authToken = CookiesStorage.get("authToken");
		}

		if (CookiesStorage.get("uid")) {
			headers["User-Id"] = CookiesStorage.get("uid");
		}

		RestangularConfigurer.setDefaultHeaders(headers);
		RestangularConfigurer.setBaseUrl("/api");
		RestangularConfigurer.setErrorInterceptor(Interceptors.error);
		RestangularConfigurer.addFullRequestInterceptor(Interceptors.fullRequest);
		RestangularConfigurer.addResponseInterceptor(Interceptors.response);
	});
}
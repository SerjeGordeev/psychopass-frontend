angular
	.module("app.backend")
	.service("Interceptors", Interceptors);

function Interceptors($injector, Errors, CookiesStorage, flashAlert) {
console.log(flashAlert)
	return {
		error,
		fullRequest,
		response
	};


	function error(res) {
		switch (res.status) {

			case 500:
				flashAlert.error("500 Internal Server Error. Сервис временно недоступен. Мы уже в курсе и скоро все починим.");

				return false;

			case 401:

				flashAlert.error("Некорректное имя пользователя или пароль");

				return false;

			case 404:
			case 403:
				const extMessage = `${_.get(res, "data.message", "")} ${_.get(res, "data.errorMessage", "")}`;
				const message = `${res.status} ${res.statusText} ${extMessage}`;

				flashAlert.error(message);

				return true;

			case 422:
				flashAlert.error("422 Internal Server Error");

				return false;

			default:
				if (res.errorCode) {
					const message = Errors.get(res.errorCode) || res.errorMessage;

					flashAlert.error(message);

					return false;
				}
		}

		return true;
	}

	function fullRequest(element, operation, route, url, headers, params, httpConfig) {

		if (operation === "put") {
			if(element.oldVal){
				element.oldVal = null;
			}
		}
		if (operation === "remove") {
			return {
				headers: {
					"Content-Type": "application/json;charset=utf-8"
				},
				element: null
			};
		}
	}

	function response(data, operation, what, url, resp, deferred) {
		/*console.log("RESPONSE",data)*/
		if (data.errorCode) {
			error(data);
			deferred.reject(data);
		}
		
		if (data && data.length) {
			_.forEach(data, result=>{
				if(result._id){
					result.id = result._id
				}
			})
			return data;
		}

		if(data._id){
			data.id = data._id
		}

		return data;
	}
}
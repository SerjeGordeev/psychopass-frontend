angular
	.module("app.backend")
	.factory("AuthFactory", AuthFactory)

function AuthFactory(Api, CookiesStorage) {
	return {
		getInstance: () => {
			return Api
				.withConfig(RestangularConfigurer => {
					const headers = angular.copy(RestangularConfigurer.defaultHeaders);

					headers.authToken = CookiesStorage.get("mean-token");
					RestangularConfigurer.setDefaultHeaders(headers);
				})
				.service("/auth");
		},
		custom: (url) => {
			return Api
				.withConfig(RestangularConfigurer => {
					const headers = angular.copy(RestangularConfigurer.defaultHeaders);

					headers.authToken = CookiesStorage.get("mean-token");
					RestangularConfigurer.setDefaultHeaders(headers);
				})
				.all(url);
		}
	}
}
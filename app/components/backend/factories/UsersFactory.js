angular
	.module("app.backend")
	.factory("UsersFactory", UsersFactory)

function UsersFactory(Api, CookiesStorage) {
	return {
		getInstance: () => {
			return Api
				.withConfig(RestangularConfigurer => {
					const headers = angular.copy(RestangularConfigurer.defaultHeaders);

					headers.authToken = CookiesStorage.get("mean-token");
					RestangularConfigurer.setDefaultHeaders(headers);
				})
				.service("/users");
		},
		custom: (url) => {
			return Api
				.withConfig(RestangularConfigurer => {
					const headers = angular.copy(RestangularConfigurer.defaultHeaders);

					headers.authToken = CookiesStorage.get("mean-token");
					RestangularConfigurer.setDefaultHeaders(headers);
				})
				.all(url);
		}
	};
}
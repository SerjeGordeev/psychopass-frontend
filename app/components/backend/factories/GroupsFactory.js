angular
	.module("app.backend")
	.factory("GroupsFactory", GroupsFactory)

function GroupsFactory(Api, CookiesStorage) {
	return {
		getInstance: () => {
			return Api
				.withConfig(RestangularConfigurer => {
					const headers = angular.copy(RestangularConfigurer.defaultHeaders);

					headers.authToken = CookiesStorage.get("mean-token");
					RestangularConfigurer.setDefaultHeaders(headers);
				})
				.service("/groups");
		},
		custom: (url) => {
			return Api
				.withConfig(RestangularConfigurer => {
					const headers = angular.copy(RestangularConfigurer.defaultHeaders);

					headers.authToken = CookiesStorage.get("mean-token");
					RestangularConfigurer.setDefaultHeaders(headers);
				})
				.all(url);
		}
	}
}
angular
	.module("app.backend")
	.factory("PropertiesFactory", PropertiesFactory)

function PropertiesFactory(Api, CookiesStorage) {
	return {
		getInstance: () => {
			return Api
				.withConfig(RestangularConfigurer => {
					const headers = angular.copy(RestangularConfigurer.defaultHeaders);

					headers.authToken = CookiesStorage.get("mean-token");
					RestangularConfigurer.setDefaultHeaders(headers);
				})
				.service("/properties");
		},
		custom: (url) => {
			return Api
				.withConfig(RestangularConfigurer => {
					const headers = angular.copy(RestangularConfigurer.defaultHeaders);

					headers.authToken = CookiesStorage.get("mean-token");
					RestangularConfigurer.setDefaultHeaders(headers);
				})
				.all(url);
		}
	}
}
angular
	.module("app.backend")
	.factory("TestsFactory", TestsFactory)

function TestsFactory(Api, CookiesStorage) {
	return {
		getInstance: () => {
			return Api
				.withConfig(RestangularConfigurer => {
					const headers = angular.copy(RestangularConfigurer.defaultHeaders);

					headers.authToken = CookiesStorage.get("mean-token");
					RestangularConfigurer.setDefaultHeaders(headers);
				})
				.service("/tests");
		},
		custom: (url) => {
			return Api
				.withConfig(RestangularConfigurer => {
					const headers = angular.copy(RestangularConfigurer.defaultHeaders);

					headers.authToken = CookiesStorage.get("mean-token");
					RestangularConfigurer.setDefaultHeaders(headers);
				})
				.all(url);
		}
	};
}
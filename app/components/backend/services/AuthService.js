angular
	.module('app.backend')
	.service('Auth', Auth);

function Auth($rootScope, $q, CookiesStorage, AuthFactory, $state) {

	let Service = this;

	const decodeJWT = require('jwt-decode');

	Service.currentUser = null;

		Service.userRoles = {
			student: {
				type: "student",
				orgType: "education",
				title: "Студент",
				many: "Студенты"
			},
			admin: {
				type: "admin",
				title: "Администратор",
				many: "Администраторы"
			},
			psycholog: {
				type: "psycholog",
				orgType: "psycho",
				title: "Психолог",
				many: "Психологи"
			},
			kurator: {
				type: "kurator",
				title: "Куратор",
				many: "Кураторы"
			}
		}

	Service.api = () => AuthFactory.getInstance();

	Service.login = login;
	Service.logout = logout;
	Service.isAuth = isAuth;
	Service.signUp = signUp;

	function login(login, password) {
		return AuthFactory
			.custom("auth").all("login")
			.post({login, password})
			.then((response)=> {
				const userData = decodeJWT(response.token);
				CookiesStorage.set("mean-token", response.token);
				setUserData(userData, response.token);
				$rootScope.isAuth = Service.isAuth();
			})
	}

	function signUp(userData) {
		return AuthFactory
			.custom("auth").all("sign_up")
			.post(userData)
	}

	function logout() {
		CookiesStorage.drop("mean-token").then(()=>{
			Service.currentUser = null;
			$rootScope.isAuth = Service.isAuth();
			$state.transitionTo("auth.signIn");
		});
	}

	function isAuth() {
		if(!Service.currentUser){
			const token = CookiesStorage.get("mean-token");
			if(token) {
				setUserData(decodeJWT(token), token)
			} else {
				return false;
			}
		}

		return true;
	}

	function setUserData(userData, token) {
		console.log("Пользователь авторизован",userData,token,CookiesStorage)
		Service.currentUser = {};
		_.assign(Service.currentUser, userData);
		Service.currentUser.token = token;
		Service.currentUser.roleTitle =  Service.userRoles[userData.role].title;

	}
}
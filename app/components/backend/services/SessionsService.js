angular
	.module("app.backend")
	.service("Sessions", Sessions);


function Sessions($location, $q, md5, Api, CookiesStorage, Users) {
	const vm = this;
	const endpoint = Api.service("/sessions");


	vm.auth = auth;
	vm.drop = drop;
	vm.init = init;
	vm.isAuthorized = isAuthorized;


	/**
	 * Инициализация имеющейся сессии
	 */
	function init() {
		const token = CookiesStorage.get("authToken");

		if (Boolean(token)) {
			endpoint
				.one(token)
				.get()
				.then(data => Users.setCurrentUser(data.user))
				.catch(drop);
		}
	}

	/**
	 * Запрос на авторизацию
	 */
	function auth(login, password) {
		
		
		return endpoint
			.post({
				login,
				password: md5.createHash(password)
			})
			.then(data => $q.all([
				CookiesStorage.set("authToken", data.authToken),
				Users.setCurrentUser(data.user)
			]));
	}

	function dropEntities() {
		Users.drop();
		News.drop();
		ExternalSystems.drop();
		NewsFilter.drop();
	}

	/**
	 * Проверка аторизован ли пользователь
	 */
	function isAuthorized() {
		return Boolean(CookiesStorage.get("authToken"));
	}

	/**
	 * разлогинка
	 */
	function drop() {
		const token = CookiesStorage.get("authToken");

		return CookiesStorage
			.drop()
			.then(() => endpoint.one(token).remove())
			.then(() => {
				dropEntities();
				$location.path("/");
			});
	}
}
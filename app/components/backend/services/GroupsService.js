	angular
		.module('app.backend')
		.service('Groups', Groups);

	function Groups ($q, GroupsFactory, Auth, UsersFactory, Users) {
		let Service = this;

		Service.api = () => GroupsFactory.getInstance();

		Service.getGroups = getGroups;
		Service.getOne = getOne;

		Service.groupTypes = {
/*			education: {
				type: "education",
				title: "Образовательная"
			},
			psycho: {
				type: "psycho",
				title: "Психологическая"
			}*/
		}

		function getGroups(params){
			console.log(params)
			return $q.all({
				groups: Service.api().getList(params),
				psychologs: UsersFactory.getInstance().getList({role:"psycholog"})
			}).then(response=>{
				Service.psychologs = response.psychologs;
				response.groups = $$processGroups(response.groups);
				return response.groups
			})
		}

		function getOne(id){
			return Service.api().one(id).get().then(group=>{
				group = $$processGroups([group])[0]
				return group;
			})
		}

		function $$processGroups(groups){

			_.forEach(groups, group=>{
				group.members = Users.$$processUsers(group.members)
			});

			return _.chain(groups)
					.map(assignMentors)
					.value()

			function assignMentors(group){
				let mentorData = _.find(Service.psychologs, {group: group.id});
				group.mentorData = mentorData? mentorData : null;
				group.mentorId = mentorData? mentorData.id : null
				return group;
			}
		}

	}
	angular
		.module('app.backend')
		.service('Organisations', Organisations);

	function Organisations ($q, OrganisationsFactory, Auth, Users) {
		let Service = this;

		Service.api = () => OrganisationsFactory.getInstance();

		Service.getOrganisations = getOrganisations;
		Service.getOne = getOne;

		Service.organisationTypes = {
			education: {
				type: "education",
				title: "Образовательная"
			},
			psycho: {
				type: "psycho",
				title: "Психологическая"
			}
		}

		function getOrganisations(params){
			console.log(params)
			return Service.api().getList(params).then(organisations=>{
				organisations = $$processOrganisations(organisations);
				return organisations;
			})
		}

		function getOne(id){
			return Service.api().one(id).get().then(organisation=>{
				organisation = $$processOrganisations([organisation])[0]
				return organisation;
			})
		}

		function $$processOrganisations(organisations){

			_.forEach(organisations, org=>{
				org.members = Users.$$processUsers(org.members)
				Users.$$assignGroups(org.members).then(members=>{
					org.memebrs = members
				})
			})

			return _.chain(organisations)
					.map(assignOrganisationData)
					.value()

			function assignOrganisationData(organisation) {
				let typeData = Service.organisationTypes[organisation.type]
				if (typeData) {
					organisation.typeTitle = typeData.title;
					organisation.membersTitle = _.find(Auth.userRoles,{orgType:typeData.type}).many;
				}
				return organisation;
			}
		}

	}
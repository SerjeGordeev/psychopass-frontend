	angular
		.module('app.backend')
		.service('Properties', Properties);

	function Properties ($q, PropertiesFactory) {
		let Service = this;

		Service.api = () => PropertiesFactory.getInstance();

		Service.getProperties = getProperties;
		Service.getOne = getOne;

		Service.propertyTypes = {
			number: {
				type: "number",
				title: "Числовая"
			},
			formal: {
				type: "text",
				title: "Текстовая"
			},
		}

		function getProperties(params){
			return $q.all({
				properties: Service.api().getList(params)//,
				//psychologs: UsersFactory.getInstance().getList({role:"psycholog"})
			}).then(response=>{
				//Service.psychologs = response.psychologs;
				response.properties = $$processProperties(response.properties);
				return response.properties
			})
		}

		function getOne(id){
			return Service.api().one(id).get().then(property=>{
				property = $$processProperties([property])[0]
				return property;
			})
		}

		function $$processProperties(Properties){

			return _.chain(Properties)
					.map(assignTypes)
					.value()

			function assignTypes(property){
				property.typeTitle = _.get(Service.propertyTypes[property.type], "title", null)
				return property
			}
		}

	}
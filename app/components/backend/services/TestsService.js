	angular
		.module('app.backend')
		.service('Tests', Tests);

	function Tests ($q, TestsFactory, Properties, Users) {
		let Service = this;

		Service.api = () => TestsFactory.getInstance();

		Service.getTests = getTests;
		Service.getOne = getOne;
		Service.getResults = getResults;

		Service.testsTypes = {
			own: {
				type: "own",
				title: "Внутренний"
			},
			external: {
				type: "external",
				title: "Внешний"
			}
		}

		function getTests(params, postProcessingParams){
			return $q.all({
				tests: Service.api().getList(params),
				properties: Properties.getProperties(),
				users: _.get(postProcessingParams, "users")? Users.getUsers({role:"student"}) : null
			}).then(response=>{
				Service.users = response.users;
				Service.properties = response.properties;

				response.tests = $$processTests(response.tests);
				return $$assignAuthors(response.tests);
			})
		}

		function getOne(id, postProcessingParams){
			return $q.all({
				test: Service.api().one(id).get(),
				properties: Properties.getProperties(),
				users: _.get(postProcessingParams, "users")? Users.getUsers({role:"student"}) : null
			}).then(response=>{
				Service.users = response.users;
				Service.properties = response.properties;
				let test = $$processTests([response.test])[0];
				return $$assignAuthors([test]).then(tests=>{
					return tests[0]
				});
			});
		}

		function $$processTests(tests){

			return _.chain(tests)
					.map($$assignProperties)
					.map($$assignTypes)
					.map($$assignPassingsUsers)
					.map($$$assignPassingsMethods)
					.value();

			function $$assignProperties(test){
				test.propertyData = _.find(Service.properties, {id: test.propId}) || {};
				return test;
			}

			function $$assignTypes(test){
				test.typeTitle = _.get(Service.testsTypes[test.type], "title", null);
				return test;
			}

			function $$assignPassingsUsers(test){
				let users = _.filter(Service.users, (user) => _.find(test.passings, {memberId: user.id}));

				_.forEach(test.passings, passing=>{
					passing.memberData = _.find(users, {id: passing.memberId});
				});

				return test;
			}

			function $$$assignPassingsMethods(test){
				_.assign(test.passings, {
					getPoints: getPoints.bind(test)
				});

				return test;

				function getPoints(groupId, isUniq){
					let results = this.passings;

					if (groupId) {
						results = _.filter(results, (passing)=>_.get(passing,"memberData.group") === groupId);
					}

					if (isUniq) {
						results = _.uniqBy(results, "memberId");
					}

					return results
				}
			}
		}

		function $$assignAuthors(tests){
			let authorsIds = _.map(tests, "authorId");
			let proms = _.map(authorsIds, id => {
				return Users.getUsers({_id: id})
			});

			return $q.all(proms).then(authors=>{
				authors = _.map(authors, auth=>auth[0])
				_.forEach(tests, test=>{
					test.authorData = _.find(authors, {id: test.authorId}) || {};
				});
				return tests;
			});
		}

		function getResults(results, test){
			let summ = _.sum(_.map(results,res=>parseInt(res, 10)));
			//let rangeIndex = null;

			let range = _.find(test.translation, (range, ix)=>{
				return (summ >= range.range[0] && summ <= range.range[1]);
			});
			//console.log(range, test)
			return {
				summ,
				message: range.message,
				propValue: $$getPropValue(),
				propName: test.propertyData.name
			};

			function $$getPropValue() {
				let result;
				switch(test.translationType) {
					case "fixed":
						result = range.propValue;
						break;
					case "calculate":
						let maxBallsCount = _.last(test.translation).range[1];
						result = (summ*test.propertyData.max)/maxBallsCount;
						/*console.log(`(${summ}*${test.propertyData.max})/${maxBallsCount}`);*/
						break;
				}
				/*console.log(result, summ, test, range);*/
				return result;
			}
		}
	}
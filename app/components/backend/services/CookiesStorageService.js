angular
	.module('app.backend')
	.service('CookiesStorage', CookiesStorage)

function CookiesStorage($cookies, $q) {
	let vm = this;


	vm.drop = drop;
	vm.set = setCookies;
	vm.get = getCookies;


	function drop() {
		return $q(resolve => {
			let allCookies = $cookies.getAll()
			// drop cookies
			_.forOwn(allCookies, (val, k) => {
				$cookies.remove(k)
			})

			let a = setInterval(() => {
				clearInterval(a)
				resolve()
			}, 0)
		})
	}


	function setCookies(key, value) {
		return $q(resolve => {
			$cookies.put(key, value)

			let a = setInterval(() => {
				if (getCookies(key) == value) {
					clearInterval(a)
					resolve()
				}
			}, 0)
		})
	}


	function getCookies(key) {
		return $cookies.get(key)
	}

}

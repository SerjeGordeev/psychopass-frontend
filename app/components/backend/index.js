angular.module('app.backend',["ngCookies", "restangular", "angular-md5"])
	.config(route);

function requireAll(r) {
	r.keys().forEach(r);
}
requireAll(require.context("./", true, /\.js$/));

function route() {
}
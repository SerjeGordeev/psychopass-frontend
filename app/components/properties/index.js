angular.module('app.properties',[])
	.config(route);

function requireAll(r) {
	r.keys().forEach(r);
}
requireAll(require.context("./", true, /\.js$/));

require("./styles.scss");

function route($stateProvider) {
	$stateProvider.state({
		name: 'app.properties',
		url: '/properties',
		template: "<properties></properties>"
	})
}
angular.module('app.properties')
	.directive('properties', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./Properties.html"),
	controller: Controller,
	controllerAs: "prpsCtrl",
	scope: {}
}

function Controller($state, $scope, Users, Auth, flashAlert, Properties) {
	const vm = this;

	vm.properties = [];

	vm.propertyTypes = Properties.propertyTypes;
	vm.editMode = false;
	vm.CRUD = ["admin"]
	//console.log($state.CRUD, Auth.currentUser.role)
	vm.isCRUD = _.includes(vm.CRUD, Auth.currentUser.role)

	vm.addProperty = addProperty;
	vm.removeProperty = removeProperty;
	vm.updateProperty = updateProperty;
	// vm.openMembers = openMembers;

	init();

	function init(){
		vm.promise = Properties.getProperties()
		.then((properties) => {
			vm.properties = _.map(properties, (property) => {
				_.initUpdating(property)
				return property;
			});
		});
	}

	function addProperty() {
		vm.promise = Properties.api().post({
			name: " Новая характеристика",
			type: "number",
			min: 0,
			max: 100
		}).then(()=>{
			init();
			flashAlert.success("Новая характеристика добавлена")
		});
	}

	function removeProperty(user){
		vm.promise = user.remove().then(()=>{
			init();
			flashAlert.success(`Характеристика ${_.get(user, "name", "")} удалена`);
		});
	}

	function updateProperty(user) {
		if(_.isUpdated(user)){
			vm.promise = user.put().then(()=>{
				init();
				flashAlert.success("Данные характеристики обновлены")
			});
		}
	}

	// function openMembers(organisation){
	// 	$state.transitionTo("app.admin.organisation",{id: organisation.id})
	// }
}

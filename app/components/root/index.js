angular.module('app.root',[])
	.config(route);

function requireAll(r) {
	r.keys().forEach(r);
}
requireAll(require.context("./", true, /\.js$/));

function route($stateProvider) {
	$stateProvider.state({
		name: 'root',
		url: '/',
		template: require("./index.html"),
		onEnter: function($state, $rootScope) {
				if(!$rootScope.isAuth){
					$state.transitionTo('auth.signIn')
				} else {
					$state.transitionTo('app.about')
				}
			}
	})
}
angular.module('app.analyse')
	.directive('analyseList', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./List.html"),
	controller: Controller,
	controllerAs: "alsLtCtrl",
	scope: {}
}

function Controller($state, $scope, Users, Auth, flashAlert, Properties) {
	const vm = this;

	vm.analysePages = [];
	vm.openPage = openPage;

	init();

	function init(){
		vm.analysePages = getChildStates("app.analyse");
	}

	function openPage(page){
		$state.transitionTo(page.name);
	}

	function getChildStates(parentState) {
     	return $state.get().filter(function(cState) { 
     		return cState.name.indexOf(parentState) === 0 &&
     		 cState.name !== parentState &&
     		 cState.name !== $state.current.name;
     	});
     }
}


angular.module('app.analyse')
  .directive('gaussSkoGraph', () => DIR_PARAMS);

var DIR_PARAMS = {
  template: require("./Graph.html"),
  controller: Controller,
  controllerAs: "gsGphCtrl",
  scope: {
    prop: "=",
    type: "=",
    sigmaCoef: "="
  },
  link(scope, element, attrs, ctrl){
      ctrl.$timeout(setWidth);

      let resizeListener  = angular.element(ctrl.$window).on("resize", setWidth);
      scope.$on("$destroy", ()=>{
        angular.element(ctrl.$window).off("resize", setWidth)
      });

      function setWidth(){
        element[0].firstChild.style.width = getComputedStyle(element[0].parentNode.parentNode).width
      }

      ctrl.setWidth = setWidth;
  }
}

function Controller($q, $state, $scope, $window, $timeout, Users, Auth, flashAlert, Properties) {
  const vm = this;

    vm.prop = $scope.prop;
    vm.$window = $window;
    vm.$timeout = $timeout;

    $scope.labels = ["min","x < μ - (σ*k)", "μ - (σ*k) < x < μ + (σ*k)", "x > μ + (σ*k)","max"];

    $scope.series = ['% студентов'/*, 'Series B'*/];

    let results = getPartsArray();

    $scope.data = [
      [..._.map(results, _.round),100]/*,
      [28, 48, 40, 19, 86, 27, 90]*/
    ];

    $scope.onClick = function (points, evt) {
      console.log(points, evt);
    };

    $scope.datasetOverride = [
	    { yAxisID: 'y-axis-1' }
    ];

    $scope.options = {
      animation: {
        duration: 500,
        easing: "easeOutQuart",
        onProgress: function(){graphLabels.call(this)},
        onComplete: function(){graphLabels.call(this)}
      },
      scales: {
        yAxes: [
          {
            id: 'y-axis-1',
            type: 'linear',
            display: true,
            position: 'left'
          }/*,
          {
            id: 'y-axis-2',
            type: 'linear',
            display: true,
            position: 'right'
          }*/
        ]
      }
    };

  function graphLabels(){
    /* ДЛЯ ОТОБРАЖЕНИЯ ЗНАЧЕНИЙ */
    var ctx = this.chart.ctx;
    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
    ctx.textAlign = 'center';
    ctx.textBaseline = 'bottom';

    this.data.datasets.forEach(function (dataset) {
      for (var i = 0; i < dataset.data.length; i++) {
        var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
            scale_max = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._yScale.maxHeight;
        ctx.fillStyle = '#444';
        var y_pos = model.y - 5;
        // Make sure data value does not get overflown and hidden
        // when the bar's value is too close to max value of scale
        // Note: The y value is reverse, it counts from top down
        if ((scale_max - model.y) / scale_max >= 0.93)
          y_pos = model.y + 20;
        ctx.fillText(_.round(dataset.data[i], 2) + "%", model.x, y_pos);
      }
    });
  }

    function getPartsArray(){
      let Max = _.max(vm.prop.values);
      let values =  _.sortBy(vm.prop.values);

      return _.map($scope.labels, label => {
        let result = null;

        switch (label){
          case "min":
            result = 0;
            break;
          case "x < μ - (σ*k)":
            result = _.filter(values, val=>{
              return val < (vm.prop.mu - vm.prop.sigma*$scope.sigmaCoef)
            });
            break;
          case "μ - (σ*k) < x < μ + (σ*k)":
            result = _.filter(values, val=>{
              return (val >= (vm.prop.mu - vm.prop.sigma*$scope.sigmaCoef)) && (val <= (vm.prop.mu + vm.prop.sigma*$scope.sigmaCoef))
            });
            break;
          case "x > μ + (σ*k)":
            result = _.filter(values, val=>{
              return val > (vm.prop.mu + vm.prop.sigma*$scope.sigmaCoef)
            });
            break;
          case "max":
            //result = 100;
            break;
        }

       // result = result?result.length:0/values.length;
       // console.log(result?result.length:0,values.length, (result?result.length:0)/values.length)
        return ((result?result.length:0)/values.length)*100
      });
    }

}

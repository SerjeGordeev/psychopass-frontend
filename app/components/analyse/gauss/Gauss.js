angular.module('app.analyse')
	.directive('gauss', () => DIR_PARAMS);

require("./styles.scss")

var DIR_PARAMS = {
	template: require("./Gauss.html"),
	controller: Controller,
	controllerAs: "gsCtrl",
	scope: {
		users: "="
	}
}

const PIRSONS  = require("./pirsons.js");

function Controller($q, $state, $scope, Users, Auth, flashAlert, Properties) {
	const vm = this;

	vm.properties = [];
	vm.results = [];
	vm.graphType="chart-line";
	vm.sigmaCoef = 1;
	vm.alpha = 0.01;
	vm.getResults = getResults;

	init(); 

	function init(){

		let params = $$getQueryParams();

		vm.promise = $q.all({
			users: Users.api().getList(params),
			properties: Properties.api().getList({type:"number"})
		}).then(response=>{
			vm.users = response.users;
			vm.properties = response.properties;
			getResults();
		});		
	}

	function getResults(){
		vm.results = [];
		_.forEach(vm.properties, prop => {
			let result = {}

			_.assign(result,{
				propId: prop._id,
				propName: prop.name,
				values: getValues(prop._id),
				showGraph: false 
			});

			result.valuesCount = result.values.length;

			if(result.values.length) {
				result.minValue = _.min(result.values);
				result.maxValue = _.max(result.values);
				result.mu = _.sum(result.values)/result.values.length;
				result.sigma = getSigma(result.values, result.mu);
				result.lower = getLowerPart(result);
				//console.log("***********")
				result.upper = getUpperPart(result);
				result.pirson = getPirson(result);
				result.S = result.values.length - 3;
				result.pirsonStat = findPirsonStat(result);
			}
			vm.results.push(result)
			//console.log(vm.results)
		});
	}

	function findPirsonStat(result){
		return PIRSONS[vm.alpha][result.S - 4]
	}
	
	function getPirson(result){

		let h = 10;
		let intervals = [0];

		let i = 1;
		while(i<=100/h){
			intervals.push(h*i);
			i+=1
		}

		let empValues = _.map(intervals, (interval, ix)=>{
			if(ix+1 === intervals.length) {
				return null
			}

			return _.filter(result.values, val=>{
				return val >= interval && val <= intervals[ix+1]
			}).length
		});
		empValues.pop();

		let theoryRawValues = _.map(intervals, (interval, ix)=>{
			if(ix+1 === intervals.length) {
				return null
			}
			return (interval+intervals[ix+1])/2;
		});
		theoryRawValues.pop();

		let theoryMu = _.sum(theoryRawValues)/theoryRawValues.length;
		let theorySigma = getSigma(theoryRawValues, theoryMu);

		let T = theoryRawValues.map(val=>(val-theoryMu)/theorySigma);

		let theoryValues =_.map(T, (t, index)=>{
			let y = (1/Math.sqrt(2*Math.PI)) * Math.exp(-Math.pow(t,2)/2);

			return ((_.sum(empValues)*h)/result.sigma)*(y)
		});

		result.theoryValues = theoryValues;
		result.empValues = empValues;
		result.intervals = intervals;


/*		result.theoryValues = _.map(result.values, (value, index)=>{
			let h = 1//index?Math.abs(value-result.values[index-1]):0;
			let Ti = (1/Math.sqrt(2*Math.PI))*Math.exp(-Math.pow((value-result.mu)/result.sigma,2)/2);
			return ((_.sum(result.values)*h)/result.sigma)*(Ti)
		});*/


		return _.sum(_.map(empValues, (val, ix)=>{
			return Math.pow(val-theoryValues[ix], 2)/theoryValues[ix];
		}))/100;
	}

	function $$getQueryParams(){
		let params = null
		switch(Auth.currentUser.role) {
			case "admin":
				params = {role: "student"}
				break;
			case "psycholog":
				params = {role: "student", group: Auth.currentUser.group}
				break;
			case "student":
				params = {role: "student", _id: Auth.currentUser._id}
				break;
		}

		return params
	}

	function getValues(propId){
		return _.compact(
			_.map(vm.users, user => {
				let result = _.find(user.properties, {_id: propId})
				return (result && result.data.length)? _.last(result.data).value : null
			})
		)
	}

	function getSigma(arr, median){
		let sigma = 0;
		let summ = 0;
		_.forEach(arr, val => {
			summ += Math.pow(val - median, 2);
		})

		return Math.pow(summ/arr.length, 0.5);
	}

	function getLowerPart(result){
		return (_.filter(result.values, val =>{
				/*console.log(val , result.mu , result.sigma*vm.sigmaCoef);*/
			return val < (result.mu - result.sigma*vm.sigmaCoef)
		}).length * 100)/result.values.length
	}

	function getUpperPart(result){
		return (_.filter(result.values, val =>{
			return val > (result.mu + result.sigma*vm.sigmaCoef)
		}).length * 100)/result.values.length
	}

}

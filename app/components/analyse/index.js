angular.module('app.analyse',[])
	.config(route);

function requireAll(r) {
	r.keys().forEach(r);
}
requireAll(require.context("./", true, /\.js$/));

require("./styles.scss");

function route($stateProvider) {
	$stateProvider.state({
		name: 'app.analyse',
		url: '/analyse',
		template: "<ui-view></ui-view>"
	})

	$stateProvider.state({
		name: 'app.analyse.list',
		url: '/list',
		template: "<analyse-list></analyse-list>"
	})

	$stateProvider.state({
		title: "Нормальное распределение",
		name: 'app.analyse.gauss',
		url: '/gauss',
		template: "<gauss></gauss>",
		description: "Анализ значений каждой характеристики по принципу соответствия СКО к нормальному распределению, в разрезе всей системы и отдельных групп"
	})
}
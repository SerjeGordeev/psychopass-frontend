angular.module('app.admin')
	.directive('users', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./Users.html"),
	controller: Controller,
	controllerAs: "admUsrsCtrl",
	scope: {}
}

function Controller($scope, Users, Auth, flashAlert) {
	const vm = this;

	vm.users = [];

	vm.userRoles = Auth.userRoles;
	vm.editMode = false;
	
	vm.icons = {
		/*deleteUser: require("images/icons/ic_close_black_24px.svg"),*/
		editMode: require("images/icons/ic_mode_edit_black_24px.svg")
	}

	vm.addUser = addUser;
	vm.removeUser = removeUser;
	vm.updateUser = updateUser;
	vm.ping = function(e){console.log(e.target)};
/*
	$scope.$watchCollection(() => vm.users, (nV, oV) => {
		console.log(nV.name, oV.name)
	}, true);*/

	init();

	function init(){
		vm.promise = Users.getUsers({
			with_auth_data: true
		}).then((users) => {
			vm.users = _.map(users, (user) => {
				user.oldVal = _.clone(user);
				return user;
			});
		})
	}

	function addUser() {
		vm.promise = Users.addUser().then(()=>{
			init();
			flashAlert.success("Новый пользователь добавлен")
		});
	}

	function removeUser(user){
		vm.promise = user.remove().then(()=>{
			init();
			flashAlert.success(`Пользователь ${user.name} удален`)
		});
	}

	function updateUser(user) {

		if(isUpdated(user)){
			vm.promise = user.put().then(()=>{
				init();
				flashAlert.success("Данные пользователя обновлены")
			});
		}

		function isUpdated(obj) {
			let result = false;
			_.forIn(obj, (val, key) => {
				if((key !== "oldVal" && key !== "$$hashKey")){
					if(obj.oldVal[key] !== val || obj.oldVal[key] === undefined) {
						console.log(key)
						result = true;
					}
				}
			});

			return result
		}
	}
}

angular.module('app.admin')
	.directive('psychologs', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./Psychologs.html"),
	controller: Controller,
	controllerAs: "pshsCtrl",
	scope: {}
}

function Controller($q, $scope, $state, Organisations, Users, flashAlert, Groups) {
	const vm = this;

	vm.members = [];
	vm.organisations = [];

	vm.organisationTypes = Organisations.organisationTypes;
	vm.genderTypes = Users.genderTypes;
	vm.courses = Users.courses;
	vm.groups = [];
	vm.editMode = false;

	vm.updateMember = updateMember;
	vm.removeMember = removeMember;
	vm.addMember = addMember;

	vm.$$groupIsFree = $$groupIsFree;

	init();

	function init(){
		console.log("INIT")
		vm.promise = $q.all({
			users:  Users.getUsers({
				role: "psycholog"
			}),
			groups: Groups.api().getList(),
			organisations: Organisations.api().getList({
				type: "psycho"
			})
		}).then(response=>{
			vm.members = response.users;
			vm.groups = response.groups;
			vm.organisations = response.organisations;

			_.forEach(vm.members, (member)=>{
				_.initUpdating(member);
				member.organisationData = _.find(vm.organisations, {id: member.organisation});
				//console.log(member)
			});

			console.log(vm)
		})
	}

	function $$groupIsFree(group, psycholog){
		return psycholog.group == group.id || 
				!_.find(vm.members, {group: group.id}) 
	}

	function updateMember(member, ev){
		if(_.isUpdated(member)){
			vm.promise = Users.api().one(member._id).get().then(user=>{
				_.assign(user, member)
				return user.put()
			}).then(()=>{
				init();
				flashAlert.success(`Данные пользователя ${member.name} обновлены`)
			})
		}
		//vm.promise = Users.one()
	}

	function removeMember(user){
		Users.custom().customDELETE(user._id)
		.then(()=>{
			init();
			flashAlert.success(`Пользователь ${user.name} удален`)
		});
	}

	function addMember() {
		vm.promise = Users.api().post({
			role: "psycholog"
		}).then(()=>{
			init();
			flashAlert.success("Новый пользователь добавлен")
		});
	}
}

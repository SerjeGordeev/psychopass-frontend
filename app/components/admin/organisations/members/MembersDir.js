angular.module('app.admin')
	.directive('organisation', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./Members.html"),
	controller: Controller,
	controllerAs: "orgMbrsCtrl",
	scope: {}
}

function Controller($q, $scope, $state, Organisations, Users, flashAlert, Groups) {
	const vm = this;

	vm.organisation = null;

	vm.organisationTypes = Organisations.organisationTypes;
	vm.genderTypes = Users.genderTypes;
	vm.courses = Users.courses;
	vm.groups = [];
	vm.editMode = false;

/*	vm.addOrganisation = addOrganisation;
	vm.removeOrganisation = removeOrganisation;
	vm.updateOrganisation = updateOrganisation;
	vm.openMembers = openMembers;*/

	vm.updateMember = updateMember;
	vm.removeMember = removeMember;
	vm.addMember = addMember;

	init();

	function init(){
		vm.promise = $q.all({
			organisation:  Organisations.getOne($state.params.id),
			groups: Groups.api().getList()
		}).then(response=>{
			vm.organisation = response.organisation;
			vm.groups = response.groups;
			
			_.forEach(vm.organisation.members, _.initUpdating);
		})
	}

	function updateMember(member, ev){
		if(_.isUpdated(member)){
			vm.promise = Users.api().one(member._id).get().then(user=>{
				_.assign(user, member)
				return user.put()
			}).then(()=>{
				flashAlert.success(`Данные пользователя ${member.name} обновлены`)
			})
		}
		//vm.promise = Users.one()
	}

	function removeMember(user){
		Users.custom().customDELETE(user._id)
		.then(()=>{
			init();
			flashAlert.success(`Пользователь ${user.name} удален`)
		});
	}

	function addMember() {
		vm.promise = Users.api().post({
			organisation: vm.organisation.id
		}).then(()=>{
			init();
			flashAlert.success("Новая пользователь добавлен")
		});
	}

/*	function addOrganisation() {
		vm.promise = Organisations.addOrganisation().then(()=>{
			init();
			flashAlert.success("Новая организация добавлена")
		});
	}

	function removeOrganisation(user){
		vm.promise = user.remove().then(()=>{
			init();
			flashAlert.success(`Организация ${user.name} удалена`)
		});
	}

	function updateOrganisation(user) {

		if(isUpdated(user)){
			vm.promise = user.put().then(()=>{
				init();
				flashAlert.success("Данные организации обновлены")
			});
		}

		function isUpdated(obj) {
			let result = false;
			_.forIn(obj, (val, key) => {
				if((key !== "oldVal" && key !== "$$hashKey")){
					if(obj.oldVal[key] !== val || obj.oldVal[key] === undefined) {
						console.log(key)
						result = true;
					}
				}
			});

			return result
		}
	}

	function openMembers(organisation){
		$state.transitionTo("app.admin.organisation",{id: organisation.id})
	}*/
}

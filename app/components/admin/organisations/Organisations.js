angular.module('app.admin')
	.directive('organisations', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./Organisations.html"),
	controller: Controller,
	controllerAs: "admOrgsCtrl",
	scope: {}
}

function Controller($q, $scope, $state, Organisations, Auth, flashAlert) {
	const vm = this;

	vm.icons = {
		upload: require("images/icons/ic_file_upload_black_24px.svg")
	}

	vm.organisations = [];
	vm.csv = {
		separator: ",",
		uploadButtonLabel: "import"
	}

	$scope.$watch(()=>vm.csv, (nV)=>{
		console.log(nV)
	},true);

	vm.organisationTypes = Organisations.organisationTypes;
	vm.editMode = false;

	vm.addOrganisation = addOrganisation;
	vm.removeOrganisation = removeOrganisation;
	vm.updateOrganisation = updateOrganisation;
	vm.openMembers = openMembers;
	vm.importCSV = importCSV;

	init();

	function init(){
		vm.promise = Organisations.getOrganisations({
			with_members: true
		})
		.then((organisations) => {
			vm.organisations = _.map(organisations, (user) => {
				_.initUpdating(user)
				return user;
			});
		});
	}

	function addOrganisation() {
		vm.promise = Organisations.api().post({}).then(()=>{
			init();
			flashAlert.success("Новая организация добавлена")
		});
	}

	function removeOrganisation(user){
		vm.promise = user.remove().then(()=>{
			init();
			flashAlert.success(`Организация ${_.get(user, "name", "")} удалена`);
		});
	}

	function updateOrganisation(org) {
		console.log(org, 118)
		if(_.isUpdated(org)){
			vm.promise = org.put().then(()=>{
				init();
				flashAlert.success("Данные организации обновлены")
			});
		}
	}

	function openMembers(organisation){
		$state.transitionTo("app.admin.organisation",{id: organisation.id})
	}

	function importCSV(){
		let head = _.toArray(vm.csv.result.shift());
		let proms = []
		console.log(head, _.intersection(head,["Название организации", "Тип организации"]).length)
		if(_.intersection(head,["Название организации", "Тип организации"]).length != head.length){
			flashAlert.error("Ошибка в структуре csv-файла 1")
		} else {
			_.forEach(vm.csv.result, newOrg=>{
				if(_.includes(["Психологическая","Образовательная"], newOrg[1])) {
					let orgToAdd = {
						name: newOrg[0],
						type: newOrg[1] == "Психологическая"?"psycho":"education"
					}

					proms.push(Organisations.api().post(orgToAdd));
					//vm.organisations.push(orgToAdd)
				} else {
					flashAlert.error("Ошибка в структуре csv-файла");
				}
			});
			$q.all(proms).then(()=>{
				flashAlert.success("Организации импортированы");
				init()
			})
		}
	}
}

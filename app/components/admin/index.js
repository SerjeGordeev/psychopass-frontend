angular.module('app.admin',[])
	.config(route);

function requireAll(r) {
	r.keys().forEach(r);
}
requireAll(require.context("./", true, /\.js$/));

require("./styles.scss");

function route($stateProvider) {
	$stateProvider.state({
		name: 'app.admin',
		url: '/admin',
		template: "<admin></admin>"
	})

	$stateProvider.state({
		name: 'app.admin.users',
		url: '/users',
		template: "<users></users>"
	})

	$stateProvider.state({
		name: 'app.admin.organisations',
		url: '/organisations',
		template: "<organisations></organisations>"
	})

	$stateProvider.state({
		name: 'app.admin.organisation',
		url: '/organisations/:id',
		template: "<organisation></organisation>"
	})

	$stateProvider.state({
		name: 'app.admin.members',
		url: '/members',
		template: "<all-members></all-members>"
	})

	$stateProvider.state({
		name: 'app.admin.psychologs',
		url: '/psychologs',
		template: "<psychologs></psychologs>"
	})

	$stateProvider.state({
		name: 'app.admin.groups',
		url: '/groups',
		template: "<groups></groups>"
	})

	$stateProvider.state({
		name: 'app.admin.group',
		url: '/groups/:id',
		template: "<group></group>"
	})
}
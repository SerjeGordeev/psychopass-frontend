angular.module('app.admin')
	.directive('allMembers', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./Members.html"),
	controller: Controller,
	controllerAs: "mbrsCtrl",
	scope: {}
}

function Controller($q, $scope, $state, Organisations, Users, flashAlert, Groups) {
	const vm = this;

	vm.members = [];
	vm.organisations = [];

	vm.organisationTypes = Organisations.organisationTypes;
	vm.genderTypes = Users.genderTypes;
	vm.courses = Users.courses;
	vm.groups = [];
	vm.editMode = false;

	vm.updateMember = updateMember;
	vm.removeMember = removeMember;
	vm.addMember = addMember;

	init();

	function init(){
		vm.promise = $q.all({
			users:  Users.getUsers({
				role: "student"
			}),
			groups: Groups.api().getList(),
			organisations: Organisations.api().getList({
				type: "education"
			})
		}).then(response=>{
			vm.members = response.users;
			vm.groups = response.groups;
			vm.organisations = response.organisations;

			_.forEach(vm.members, (member)=>{
				_.initUpdating(member);
				member.organisationData = _.find(vm.organisations, {id: member.organisation});
			});
		})
	}

	function updateMember(member, ev){
		if(_.isUpdated(member)){
			vm.promise = Users.api().one(member._id).get().then(user=>{
				_.assign(user, member)
				return user.put()
			}).then(()=>{
				flashAlert.success(`Данные пользователя ${member.name} обновлены`)
			})
		}
		//vm.promise = Users.one()
	}

	function removeMember(user){
		Users.custom().customDELETE(user._id)
		.then(()=>{
			init();
			flashAlert.success(`Пользователь ${user.name} удален`)
		});
	}

	function addMember() {
		vm.promise = Users.api().post({}).then(()=>{
			init();
			flashAlert.success("Новая пользователь добавлен")
		});
	}
}

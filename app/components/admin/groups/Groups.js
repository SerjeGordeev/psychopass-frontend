angular.module('app.admin')
	.directive('groups', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./Groups.html"),
	controller: Controller,
	controllerAs: "grpsCtrl",
	scope: {}
}

function Controller($q, $scope, $state, Organisations, Groups, Users, Auth, flashAlert, Tests, $window) {
	const vm = this;

	vm.organisations = [];
	vm.groups = [];

	vm.organisationTypes = Organisations.organisationTypes;
	vm.editMode = false;

	vm.$$psychologIsFree = $$psychologIsFree;

	vm.addGroup = addGroup;
	vm.removeGroup = removeGroup;
	vm.updateGroup = updateGroup;
	vm.openMembers = openMembers;
	vm.changeMentor = changeMentor;
	vm.downloadReport = downloadReport;

	init();

	function init(){
		vm.promise =  $q.all({
			groups: Groups.getGroups({
				with_members: true
			}),
			psychologs: Users.api().getList({
				role: "psycholog"
			}),
			tests: Tests.getTests({},{users: true})
		}).then((response) => {
			vm.tests = response.tests;
			vm.psychologs = response.psychologs;
			vm.groups = _.map(response.groups, (group) => {
				_.initUpdating(group)
				return group;
			});
		});
	}

	function $$psychologIsFree(psycholog, group){
		return !psycholog.group || 
				psycholog.group == group.id || 
				!_.find(vm.groups,{id:psycholog.group})
	}

	function addGroup() {
		vm.promise = Groups.api().post({
			name: " Новая группа"
		}).then(()=>{
			init();
			flashAlert.success("Новая группа добавлена")
		});
	}

	function removeGroup(group){
		vm.promise = group.remove().then(()=>{
			init();
			flashAlert.success(`Группа ${_.get(group, "name", "")} удалена`);
		});
	}

	function updateGroup(group) {

		if(_.isUpdated(group)){
			vm.promise = group.put().then(()=>{
				init();
				flashAlert.success("Данные группы обновлены")
			});
		}
	}

	function changeMentor(group) {

		let oldPsycholog = _.find(vm.psychologs, {id: _.get(group.mentorData,"id", null)})
		let newPsycholog = _.find(vm.psychologs, {id:group.mentorId})
		let proms = []

		if(oldPsycholog){
			oldPsycholog.group = null
			proms.push(oldPsycholog.put());
		}

		if(newPsycholog){
			newPsycholog.group = group.id
			proms.push(newPsycholog.put());
		}

		vm.promise = $q.all(proms).then(init);
	}

	function openMembers(group){
		$state.transitionTo("app.admin.group",{id: group.id})
	}

	function downloadReport(){
		let table = [getTableHead()]
		
		_.forEach(vm.groups, group=>{
			let groupData = [group.name, group.mentorData.name, group.members.length,
			 ..._.map(vm.tests, test=>test.passings.getPoints( group.id, true).length)];

			table.push(groupData);
		});
		
		return table;

		function getTableHead(){
			return ["Название",	"Руководитель", "Кол-во участников", ..._.map(vm.tests,"name")];
		}
	}
}

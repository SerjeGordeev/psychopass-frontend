angular.module('app.admin')
	.directive('group', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./Members.html"),
	controller: Controller,
	controllerAs: "grpMbrsCtrl",
	scope: {}
}

function Controller($q, $scope, $state, Organisations, Users, flashAlert, Groups, $mdDialog, Properties) {
	const vm = this;

	vm.group = null;

	vm.groupId = $state.params.id;
}

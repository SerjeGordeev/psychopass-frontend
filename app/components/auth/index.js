angular.module('app.auth',[])
	.config(route);

function requireAll(r) {
	r.keys().forEach(r);
}
requireAll(require.context("./", true, /\.js$/));

function route($stateProvider) {
	$stateProvider.state({
		name: 'auth',
		url: '/auth',
		template: require("./index.html"),
		onEnter: function($state) {
/*			if($state.current.name === "auth") {
				console.log("asd")
				$state.go("auth.signIn")
			}*/
		}
	});

	$stateProvider.state({
		name: 'auth.signIn',
		url: '/sign_in',
		template: "<sign-in></sign-in>"
	});

	$stateProvider.state({
		name: 'auth.signUp',
		url: '/sign_up',
		template: "<sign-up></sign-up>"
	});

	$stateProvider.state({
		name: 'auth.extTest',
		url: '/ext_test',
		template: "<ext-test></ext-test>"
	});
}
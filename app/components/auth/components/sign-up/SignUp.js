angular.module('app.auth')
	.directive('signUp', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./SignUp.html"),
	controller: Controller,
	controllerAs: "snUpCtrl"
};

require("./styles.scss");

function Controller($q, $scope, $state, flashAlert, Auth, Organisations, Moment) {
	const vm = this;

	vm.organisations = [];
	vm.formData = {};
	vm.signUp = signUp;

	init();
	function init(){
		vm.promise = $q.all({
			organisations: Organisations.getOrganisations()
		}).then(response=>{
			vm.organisations = _.filter(response.organisations, {type: "education"});
			console.log(vm.organisations)
		})
	}

	function signUp(){

		let userData = _.assign(vm.formData, {
		 							birthsday: Moment
		 								.moment(vm.formData.birthsday,"YYYY-MM-DD")
										.format("DD.MM.YYYY")
									});

		vm.promise = Auth.signUp(userData)
		.then(response=>{
			flashAlert.success("Вы успешно зарегистрированы в системе. Пароль для входа отправлен на ваш адрес электронной почты.")
			$state.go("auth.signIn")	
		});
		
	}
}

angular.module('app.auth')
	.directive('extTest', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./ExtTest.html"),
	controller: Controller,
	controllerAs: "extTstCtrl"
}

require("./styles.scss");

function Controller($state, Auth) {
	const vm = this;
	
	vm.signIn = signIn;
	vm.goToExtTest = goToExtTest;

	vm.formData = {
		login: null,
		password: null
	}
	
	function signIn() {
		Auth.login(vm.formData.login, vm.formData.password).then(resp => {
			$state.transitionTo("app.about")
		});
	}

	function goToExtTest(){
		$state.transitionTo("addExtTest")
	}
}

angular.module('app.mentoring',[])
	.config(route);

function requireAll(r) {
	r.keys().forEach(r);
}
requireAll(require.context("./", true, /\.js$/));

//require("./styles.scss");

function route($stateProvider) {
	$stateProvider.state({
		name: 'app.mentoring',
		url: '/mentoring',
		template: "<mentoring></mentoring>"
	})
}
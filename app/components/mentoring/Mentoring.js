angular.module('app.mentoring')
	.directive('mentoring', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./Mentoring.html"),
	controller: Controller,
	controllerAs: "mntgCtrl",
	scope: {}
}

function Controller($q, $scope, $state, Moment, Organisations, Auth, Users, flashAlert, Groups, Properties) {
	const vm = this;

	vm.group = null;

	vm.groupId = Auth.currentUser.group;
}

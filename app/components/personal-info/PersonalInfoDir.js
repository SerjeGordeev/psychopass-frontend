angular.module('app.personal-info')
	.directive('personalInfo', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./PersonalInfo.html"),
	controller: Controller,
	controllerAs: "prsInfCtrl",
	scope: {}
}

function Controller($q, $state, $scope, Users,
 Auth, flashAlert, Properties, Groups, Organisations) {
	let vm = this;

	vm.user = null;

	vm.showUserInfo = true;
	vm.showPropsInfo = true;
	vm.showPropsDynamic = false;

	vm.doShowPropsDynamic = doShowPropsDynamic;

	init();
	function init(){
		//cnsole.log(Auth.currentUser)
		Users.getUsers({_id: Auth.currentUser._id}).then(user=>{
			vm.user = user[0];
			console.log(vm.user)
		})
		// vm.promise = $q.all({
		// 	group: Groups.getOne(Auth.currentUser.group)

		// }).then(()=>{

		// });
	}

	function doShowPropsDynamic(){
		vm.showPropsDynamic = !vm.showPropsDynamic;
		// vm.showPropsInfo = true;
		// vm.showUserInfo = !vm.showUserInfo;
	}

}

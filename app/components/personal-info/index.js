angular.module('app.personal-info',[])
	.config(route);

function requireAll(r) {
	r.keys().forEach(r);
}
requireAll(require.context("./", true, /\.js$/));

require("./styles.scss");

function route($stateProvider) {
	$stateProvider.state({
		name: 'app.personal-info',
		url: '/personal-info',
		template: "<personal-info></personal-info>"
	})
}
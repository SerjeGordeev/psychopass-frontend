angular.module('app.common')
	.directive('viewLoader', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./ViewLoader.html"),
	controller: Controller,
	controllerAs: "vwLdrCtrl"
};

require("./styles.scss");

function Controller($scope, $rootScope) {
	const vm = this;
	
	vm.appName = $rootScope.appName;
}

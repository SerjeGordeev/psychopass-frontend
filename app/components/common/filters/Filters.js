angular.module('app.common')
	.filter('Empty', function(){
		return function(item, label) {
			return item? item : 
								label? label : "Неизвестно";
		}
	})
	.filter('Round', function(){
		return function(val, precision = 2) {
			return _.round(val, precision);
		}
	})
	.filter('Prc', function(){
		return function(val, label) {
			label = label?label:""
			return	!_.isNaN(val) && !_.isUndefined(val)? parseFloat(val,10).toFixed(2) + label: 0
		}
	})


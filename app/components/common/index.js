angular.module('app.common',[])
	.config(config);

function requireAll(r) {
	r.keys().forEach(r);
}
requireAll(require.context("./", true, /\.js$/));

function config() {

}
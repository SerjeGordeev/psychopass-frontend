angular.module('app.common')
	.directive('appMenu', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./AppMenu.html"),
	controller: Controller,
	controllerAs: "appMnCtrl"
};

require("./styles.scss");

function Controller($rootScope, $timeout, Auth, $state) {
	const vm = this;

	vm.goTo = goTo;
	vm.isActive = isActive;
	vm.squashItems = squashItems;
	vm.expandActiveItem = expandActiveItem;

	vm.showMenu = true;

	vm.menuItems = [
		{
			title: "Личная карточка",
			state: "app.personal-info",
			access: ["student"],
			image: require("images/icons/ic_account_circle_black_24px.svg")
		},
		{
			title: "Характеристики",
			state: "app.properties",
			access: ["admin", "psycholog"],
			editRights: ["admin"],
			image: require("images/icons/ic_assessment_black_24px.svg")
		},
		{
			title: "Руководство группой",
			state: "app.mentoring",
			access: ["psycholog"],
			image: require("images/icons/ic_group_black_24px.svg")
		},
/*		{
			title: "Группы",
			state: "app.groups",
			access: ["admin"],
			image: require("images/icons/ic_group_black_24px.svg")
		},*/
/*		{
			title: "Организации",
			state: "app.organisations",
			access: ["admin"],
			image: require("images/icons/ic_account_balance_black_24px.svg")
		},*/
		{
			title: "Тесты",
			state: "app.tests",
			access: ["admin","psycholog","student"],
			image: require("images/icons/ic_assignment_black_24px.svg")
		},
/*		{
			title: "Участники",
			state: "app.members",
			access: ["admin"],
			image: require("images/icons/ic_assignment_black_24px.svg")
		},*/
/*		{
			title: "Психологи",
			state: "app.psychologs",
			access: ["admin"],
			image: require("images/icons/ic_assignment_black_24px.svg")
		},*/
		{
			title: "Анализ",
			state: "app.analyse.list",
			access: ["admin", "psycholog", "student"],
			image: require("images/icons/ic_timeline_black_24px.svg"),
			subrouts: [
				{
					title: "Нормальное распределение",
					state: "app.analyse.gauss",
					access: null,
					image: null
				}
			]
		},
		{
			title: "Администрирование",
			//state: "app.admin",
			access: ["admin"],
			image: require("images/icons/ic_visibility_black_24px.svg"),
			nested: true,
			subrouts: [
				{
					title: "Пользователи",
					state: "app.admin.users",
					access: ["admin"],
					image: require("images/icons/ic_group_black_24px.svg")
				},
				{
					title: "Организации",
					state: "app.admin.organisations",
					access: ["admin"],
					image: require("images/icons/ic_account_balance_black_24px.svg"),
					subrouts: [
						{
							title: "Представители",
							state: "app.admin.organisation",
							access: null,
							image: null,
						}
					]
				},
				{
					title: "Группы",
					state: "app.admin.groups",
					access: ["admin"],
					image: require("images/icons/ic_group_black_24px.svg"),
					subrouts: [
						{
							title: "Участники",
							state: "app.admin.group",
							access: null,
							image: null,
						}
					]
				},
				{
					title: "Участники",
					state: "app.admin.members",
					access: ["admin"],
					image: require("images/icons/ic_group_black_24px.svg")
				},
				{
					title: "Психологи",
					state: "app.admin.psychologs",
					access: ["admin"],
					image: require("images/icons/ic_group_black_24px.svg")
				}/*,
				{
					title: "Внешние системы",
					state: "app.extenal_systems",
					access: ["admin"],
					image: require("images/icons/ic_assignment_black_24px.svg")
				}*/
			]
		},
		{
			title: "О системе",
			state: "app.about",
			access: ["admin","student","psycholog"],
			image: require("images/icons/ic_help_black_24px.svg")
		}
	];
	
	$rootScope.menuItems = vm.menuItems;

	vm.icons = {
		menu: require("images/icons/ic_menu_black_24px.svg")
	}

	init();

	function init(){
		vm.menuItems = _.filter(vm.menuItems,(item) => {
			return _.includes(item.access, Auth.currentUser.role);
		});
	}

	function goTo(menuItem) {
		if(!isActive(menuItem) || menuItem.subrouts.length) {
			$state.go(menuItem.state)
			vm.showMenu = false;
			_.forEach(vm.menuItems, item=>{
				item.isExpanded = false;
			})			
		}
	}

	function isActive(menuItem){
			if(menuItem.subrouts){
				let activeSubRoute = _.find(menuItem.subrouts,{state: $state.current.name})
				//console.log(activeSubRoute)
				menuItem.isActive = activeSubRoute? !activeSubRoute.image : $state.current.name === menuItem.state
			} else{
				menuItem.isActive = $state.current.name === menuItem.state;
			}
			return menuItem.isActive /*|| _.find(menuItem.subrouts,{isActive:true});*/
	}

	function squashItems(){
		_.forEach(vm.menuItems, item=>{
			$timeout(()=>{item.isExpanded = false;},200)
			//console.log("SQUASH",item)
		})
	}

	function expandActiveItem(){
		let activeNested = _.chain(vm.menuItems)
							.filter(item=>{
								return item.subrouts
							})
							.find(item=> _.find(item.subrouts,{isActive:true}))
							.value();
							//console.log(activeNested)
		if (activeNested) {
			activeNested.isExpanded = true;
			console.log("EXPAND")
		}
	}

}

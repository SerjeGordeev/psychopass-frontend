angular.module('app.common')
	.directive('statusLabel', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./StatusLabel.html"),
	controller: Controller,
	controllerAs: "stLbCtrl",
	scope: {
		status: "="
	}
};

require("./styles.scss");

function Controller($scope) {
	const vm = this;
	console.log($scope.status)
	$scope.createTicketData = vm.formData;

}

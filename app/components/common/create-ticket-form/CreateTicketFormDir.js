angular.module('app.common')
	.directive('createTicketForm', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./CreateTicketForm.html"),
	controller: Controller,
	controllerAs: "crtTktCtrl",
	scope: {
		isExternal: "=",
		createTicketData: "="
	},
	replace: true
};

require("./styles.scss");

function Controller($scope) {
	const vm = this;


	vm.formData = {
		declarantName: null,
		declarantPhone: null,
		description: null,
		adress: null
	}

	$scope.createTicketData = vm.formData;

}

angular.module('app.common')
	.directive('hideButton', () => DIR_PARAMS);


var DIR_PARAMS = {
	template: require("./HideButton.html"),
	controller: Controller,
	controllerAs: "hdBtnCtrl",
	scope: {
		title: "@",
		isActive: "=",
		isDisabled: "=",
		colorize: "=?",
		withIcon: "=?"
	}
};

function Controller($scope) {
	const vm = this;

	vm.icons = {
		showed: require("images/icons/ic_keyboard_arrow_down_black_24px.svg"),
		hidden: require("images/icons/ic_keyboard_arrow_up_black_24px.svg")
	}

	$scope.withIcon = _.isUndefined($scope.withIcon)?true:$scope.withIcon;
	$scope.colorize = _.isUndefined($scope.colorize)?true:$scope.colorize;
}

/*editMode: require("images/icons/ic_mode_edit_black_24px.svg")*/
angular.module('app.common')
	.directive('editButton', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./EditButton.html"),
	controller: Controller,
	controllerAs: "edtBtnCtrl",
	scope: {
		title: "@",
		isActive: "=",
		isDisabled: "=",
		colorize: "=?",
		withIcon: "=?"
	}
};

function Controller($scope) {
	const vm = this;

	vm.icons = {
		edit: require("images/icons/ic_mode_edit_black_24px.svg")
	}

	$scope.withIcon = _.isUndefined($scope.withIcon)?true:$scope.withIcon;
	$scope.colorize = _.isUndefined($scope.colorize)?true:$scope.colorize;
}

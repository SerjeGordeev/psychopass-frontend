angular.module('app.common')
	.directive('addButton', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./AddButton.html"),
	controller: Controller,
	controllerAs: "addBtnCtrl",
	scope: {
		title: "@",
		isDisabled: "=",
		colorize: "=?"
	}
};

function Controller($scope) {
	const vm = this;

	vm.icons = {
		add: require("images/icons/ic_add_black_24px.svg")
	}
	
	$scope.colorize = _.isUndefined($scope.colorize)?true:$scope.colorize;
}

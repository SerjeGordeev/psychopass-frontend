angular.module('app.common')
	.directive('removeButton', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./RemoveButton.html"),
	controller: Controller,
	controllerAs: "rmBtnCtrl",
	scope: {
		title: "@",
		isDisabled: "=",
		colorize: "=?"
	}
};

function Controller($scope) {
	const vm = this;

	vm.icons = {
		remove: require("images/icons/ic_clear_black_24px.svg")
	}

	$scope.colorize = _.isUndefined($scope.colorize)?true:$scope.colorize;
}

angular.module('app.common')
	.directive('downloadButton', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./DownloadButton.html"),
	controller: Controller,
	controllerAs: "dwnldBtnCtrl",
	scope: {
		title: "@",
		isDisabled: "=",
		colorize: "=?"
	}
};

function Controller($scope) {
	const vm = this;

	vm.icons = {
		download: require("images/icons/ic_file_download_black_24px.svg")
	}
	
	$scope.colorize = _.isUndefined($scope.colorize)?true:$scope.colorize;
}

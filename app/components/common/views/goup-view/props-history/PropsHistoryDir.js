angular.module('app.common')
	.directive('propsHistory', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./PropsHistory.html"),
	controller: Controller,
	controllerAs: "prpsHstCtrl",
	scope: {
		propsData: "=",
		memberData: "="
	},
	link(scope, element, attrs, ctrl){
		ctrl.$timeout(setWidth);

		let resizeListener  = angular.element(ctrl.$window).on("resize", setWidth);
		scope.$on("$destroy", ()=>{
			angular.element(ctrl.$window).off("resize", setWidth)
		});

		function setWidth(){
			element[0].firstChild.style.width = getComputedStyle(element[0].parentNode.parentNode).width
		}

		ctrl.setWidth = setWidth;
	}
}

require("./styles.scss");

function Controller($q, $scope, $window, $timeout, Properties, flashAlert) {
	const vm = this;
	vm.$window  = $window;
	vm.$timeout = $timeout;

	vm.pointsCounts = [];
	vm.cellOptions = {
		remove: {
			onClick: removePoint,
			confirm: true
		},
		edit: {
			onClick: updatePoint,
			confirm: true
		}
	};

	vm.propOptions = {
		remove: {
			message: "Очистить историю",
			onClick: clearHistory,
			confirm: true
		}
	};

	$scope.$watch("propsData", (nV,oV)=>{
		setPointsConunts();
		vm.setWidth();
	}, true)

	init();
	function init() {
		setPointsConunts();
	}

	function setPointsConunts(){
		let maxCount = _.chain($scope.propsData).map("data").map("length").max().value();
		vm.pointsCounts = _.range(1, maxCount+1);
	}

	function removePoint(cellData){
		console.log(cellData)
		//point = null;
		let propData = $scope.memberData.properties[cellData.propIndex]
		//console.log(propData)
		if(cellData.point.actually && propData.data[cellData.pointIndex-1]){
			//propData.data[cellData.pointIndex-1].actually = true;
			propData.actuallVal = propData.data[cellData.pointIndex-1];
			propData.actuallVal.actually = true;
		}
		propData.data.splice(cellData.pointIndex,1)

		saveChanges("Точка удалена")
	}

	function updatePoint(cellData){
		saveChanges("Данные обновлены")
	}

	function clearHistory(cellData){
		let prop = $scope.memberData.properties[cellData.propIndex]
		prop.data = [];
		saveChanges(`История изменений характеристики ${prop.propData.name} удалена`)
	}

	function saveChanges(message){
		vm.promise = $scope.memberData.put().then(()=>{
			flashAlert.success(message)
		})
	}
}

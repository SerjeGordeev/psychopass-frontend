angular.module('app.common')
	.directive('dynamicLabel', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./DynamicLabel.html"),
	controller: Controller,
	controllerAs: "dnmLbCtrl",
	scope: {
		value: "="
	}
}

require("./styles.scss");

function Controller($scope) {
	const vm = this;

	vm.direction = null;

	vm.icons = {
		up: require("images/icons/ic_arrow_drop_up_black_24px.svg"),
		down: require("images/icons/ic_arrow_drop_down_black_24px.svg")
	}

	$scope.$watch("value", (nV)=>{
		//console.log(nV)
		vm.direction = nV >= 0?"up":"down";
	})

	init()
	function init(){
		vm.direction = $scope.value >= 0?"up":"down";
	}
}

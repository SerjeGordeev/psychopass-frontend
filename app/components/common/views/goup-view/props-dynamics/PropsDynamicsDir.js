angular.module('app.common')
	.directive('propsDynamics', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./PropsDynamics.html"),
	controller: Controller,
	controllerAs: "prpsDnmcsCtrl",
	scope: {
		propsData: "="
	},
	link(scope, element, attrs, ctrl){
		setWidth();

		let resizeListener  = angular.element(ctrl.$window).on("resize", setWidth);
		scope.$on("$destroy", ()=>{
			angular.element(ctrl.$window).off("resize", setWidth)
		});

		function setWidth(){
			element[0].firstChild.style.width = getComputedStyle(element[0].parentNode.parentNode).width
		}
	}
}

require("./styles.scss");

function Controller($q, $scope, $window, Properties) {
	const vm = this;
	vm.$window  = $window;
}

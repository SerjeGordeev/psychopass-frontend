angular.module('app.common')
	.directive('propDynamic', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./PropDynamic.html"),
	controller: Controller,
	controllerAs: "prpDnmcCtrl",
	scope: {
		prop: "="
	}
}

require("./styles.scss");

function Controller($q, $scope, Properties, Moment) {
	const vm = this;
	const moment = Moment.moment;

	vm.chartData = {}

	if($scope.prop.data.length){
		init();
	}

	function init(){

		vm.timeline = setTimeline();

		$scope.labels = [..._.map(vm.timeline, "date")];
	    
	    $scope.series = ["значение характеристики"];
	    
	    $scope.data = [
	    	[..._.map(vm.timeline, "value")]
	    ];

	    $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }];
	    
	    $scope.options = {
	      scales: {
	        yAxes: [
	          {
	            id: 'y-axis-1',
	            type: 'linear',
	            display: true,
	            position: 'left'
	          }
	        ]
	      }
	    };
	}

	function setTimeline(){
		let timeline = [];

		timeline.push({
			date: moment(_.first($scope.prop.data).date, "DD.MM.YYYY")
					.add(-1,"days")
					.format("DD.MM.YYYY"),
			value: _.first($scope.prop.data).value
		});
	
		_.forEach(_.drop($scope.prop.data), (pValue, ix) =>{

			while(
				moment(_.last(timeline).date, "DD.MM.YYYY")
				.add(1, "days")
				.isBefore(moment(pValue.date, "DD.MM.YYYY"))){
				//console.log(moment(_.last(timeline).date, "DD.MM.YYYY"))
				timeline.push({
					date: moment(_.last(timeline).date, "DD.MM.YYYY")
							.add(1, "days")
							.format("DD.MM"),
					value: _.last(timeline).value
				});
			}

			timeline.push(pValue);

		});

		timeline.push({
			date: moment().format("DD.MM.YYYY"),
			value: _.last($scope.prop.data).value
		});

		return timeline;
	}
}

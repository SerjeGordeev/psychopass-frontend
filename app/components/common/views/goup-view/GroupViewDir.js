angular.module('app.common')
	.directive('groupView', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./GroupView.html"),
	controller: Controller,
	controllerAs: "grpVwCtrl",
	scope: {
		groupId: "@",
		membersEdit: "="
	}
}

require("./styles.scss");

function Controller($q, $scope, $state, Moment, Organisations, 
	Auth, Users, flashAlert, Groups, Properties, $mdDialog) {
	const vm = this;

	vm.group = null;

	vm.organisationTypes = Organisations.organisationTypes;
	vm.genderTypes = Users.genderTypes;
	vm.courses = Users.courses;
	vm.organisations = [];
	vm.editMode = false;
	vm.showProps = false;
	
	vm.isUpdated = isUpdated;
	vm.updatePropsVals = updatePropsVals;

	vm.updateMember = updateMember;
	vm.removeMember = removeMember;
	vm.showMembersManage = showMembersManage;

	vm.membersCRUD = Auth.currentUser.role === "admin";


	init();

	function init(){
		vm.promise = $q.all({
			group: Groups.getOne($scope.groupId),
			organisations: Organisations.api().getList(),
			properties: Properties.api().getList()
		}).then(response=>{
			vm.group = response.group;
			vm.organisations = response.organisations;
			vm.properties = response.properties;
			
			return Users.getUsers({group: vm.group.id, role: "student"})
		}).then(members=>{
			vm.group.members = _.map(members, (member)=>{
   				_.initUpdating(member);
   				member.organisationData = _.find(vm.organisations, {id: member.organisation});
   				member = Users.$$assignPropsVals(member, vm.properties);
   				return member
   			});
		});
	}

	/*
		########## PROPS EDIT FUNCTIONS ##############
	*/

	function updatePropsVals(member){
		_.forEach(member.properties, (prop, ix)=>{
			if(prop.newVal) {

				_.forEach(prop.data, val=>{
					val.actually = false
				})

				//console.log(_.last(prop.data).date === Moment.moment().format("DD.MM.YYYY"));
				if (_.last(prop.data) && _.last(prop.data).date === Moment.moment().format("DD.MM.YYYY")) {
					prop.data = _.dropRight(prop.data)
				}

				prop.data.push({
					actually: true,
					date: Moment.moment().format("DD.MM.YYYY"),
					value: prop.newVal
				});

				prop.actuallVal.value = prop.newVal;
				prop.newVal = null;
			}
		})
		
		member.put().then(()=>{
			flashAlert.success("Значение обновлено")
			//init()
		});
	}

	function isUpdated(member){
		return _.find(member.properties, prop=>{
			return prop.newVal
		})
	}

	/*
		######### MEMBERS EDIT FUNCTIONS ##########
	*/

	function updateMember(member, ev){
		if(_.isUpdated(member)){
			vm.promise = Users.api().one(member._id).get().then(user=>{
				_.assign(user, member)
				return user.put()
			}).then(()=>{
				flashAlert.success(`Данные пользователя ${member.name} обновлены`)
			})
		}
	}

	function removeMember(member){
		member.group = null;
		vm.promise = member.put().then(()=>{
			init();
			flashAlert.success(`Участник ${member.name} удален из группы`)
		});
	}

	function showMembersManage(ev){
	    $mdDialog.show({
	      controller: DialogController,
	      template: require("./AssignMembersDialog.html"),//require("./AssignMembersDialog.html"),
	      parent: angular.element(document.body),
	      targetEvent: ev,
	      clickOutsideToClose:true//,
	      //fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
	    })
	    .then(function(students) {
	    	let proms = []
	    	_.chain(students)
	    	.filter({checked: true}).value()
	    	.forEach(student=>{
	    		console.log(student)
	    		student.group = vm.group.id;
	    		proms.push(student.put())
	    	})

	    	if(proms.length){
		    	vm.promise = $q.all(proms).then(()=>{
		    		flashAlert.success("Участники успешно добавлены в группу")
		    		init()
		    	})
	    	}
	    });

	   function DialogController($scope, $mdDialog) {

	   	$scope.promise = Users.getUsers({role:"student"}).then(users=>{
	   		$scope.students = _.filter(users, user => {
	   			console.log(user)
	   			return !user.group
	   		});
	   		console.log($scope.students)
	   	})

	    $scope.hide = function() {
	      $mdDialog.hide();
	    };

	    $scope.cancel = function() {
	      $mdDialog.cancel();
	    };

	    $scope.answer = function() {
	      $mdDialog.hide($scope.students);
	    };
	  }
	}
}

angular.module('app.common')
	.directive('ticketsInfoTable', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./TicketsInfoTable.html"),
	controller: Controller,
	controllerAs: "tksInfTbCtrl",
	scope: {
		tickets: "="
	}
};

require("./styles.scss");

function Controller($scope) {
	const vm = this;

	vm.tickets = $scope.tickets;
	vm.tableData = {};

	$scope.$watch(()=>$scope.tickets, (nV)=>{
		vm.tickets = nV;
		init();
	});

	init();

	function init() {
		vm.tableData = {
			_new: _.filter(vm.tickets, {status:"new"}).length,
			in_progress: _.filter(vm.tickets, {status:"in_progress"}).length,
			solved: _.filter(vm.tickets, {status:"solved"}).length
		};
	}
}

angular.module('app.common')
	.directive('userInfo', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./UserInfo.html"),
	controller: Controller,
	controllerAs: "usrInfCtrl"
};

require("./styles.scss");

function Controller($scope, Auth) {
	const vm = this;
	
	vm.currentUser = Auth.currentUser;

	vm.logout = Auth.logout;

	vm.icons = {
		exit: require("images/icons/logout.svg"),
		avatar: require("images/icons/ic_account_circle_black_24px.svg")
	};

}

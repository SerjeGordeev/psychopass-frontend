angular.module('app.common')
	.directive('appLogo', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./AppLogo.html"),
	controller: Controller,
	controllerAs: "appLgCtrl"
};

require("./styles.scss");

function Controller($rootScope) {
	const vm = this;
	vm.appName = $rootScope.appName;
}

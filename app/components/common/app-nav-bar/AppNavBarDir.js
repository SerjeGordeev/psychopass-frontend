angular.module('app.common')
	.directive('appNavBar', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./AppNavBar.html"),
	controller: Controller,
	controllerAs: "apNvBrCtrl"
};

require("./styles.scss");

function Controller($rootScope, $scope, $state) {
	const vm = this;

	vm.currentPath = null;
	vm.allMenuItems = allMenuItems();

	$scope.$watch(()=>$state.current, (nV)=>{
		/*console.log(nV, $state, getChildStates(nV.name))*/
		//vm.currentPath = _.find(vm.allMenuItems, {state: $state.current.name}).title
	});

	function getChildStates(parentState) {
		return $state.get().filter(function(cState) { return cState.name.indexOf(parentState) === 0 });
	}

	function allMenuItems(){
		let res = []

		_.forEach($rootScope.menuItems, item=>{
			res.push(item);
			if(item.subrouts){
				_.forEach(item.subrouts, rout=>{
					res.push(rout);
				});
			}
		});

		return res;
	}
}

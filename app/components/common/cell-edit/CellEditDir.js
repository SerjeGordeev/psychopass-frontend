angular.module('app.common')
	.directive('cellEdit', () => DIR_PARAMS);

var DIR_PARAMS = {
	//template: require("./CreateTicketForm.html"),
	controller: Controller,
	controllerAs: "clEdtCtrl",
	scope: {
		cellOptions: "=",
		cellData: "="
	},
	restrict: "A",
	link(scope, element, attrs, ctrl){
		const elem = element[0];
		elem.style.position = "relative";
		//console.log(scope)
		let appendString = `<div class='cell-toolbar light-text'>`;
		if(scope.cellOptions.edit){
				appendString += `<md-icon
				md-svg-src="{{icons.edit}}"
				ng-click='changeEditMode("edit")' 
				class='cell-button accent-icon'></md-icon>`;
		}
		if(scope.cellOptions.remove){
				appendString += `<md-icon
				md-svg-src="{{icons.remove}}"
				ng-click='changeEditMode("remove")' 
				class='cell-button warn-icon'></md-icon>`;
		}
		appendString+=`</div>`;

		appendString+=`
			<div ng-if='clEdtCtrl.editMode' class='cell-edit-input' layout='column' layout-align='center center'>
					<input ng-if='clEdtCtrl.editMode === "edit"' type='number' 
							ng-model='cellData.point.value'></input>

					<div ng-if='clEdtCtrl.editMode === "remove"'>{{cellOptions.remove.message || 'Удалить значение?'}}</div>

					<div layout='row' layout-align='center center'>
						<md-button class='md-icon-button' ng-click='saveChanges(cellData)'>
							<md-icon class='accent-icon' md-svg-src="{{icons.confirm}}"></md-icon>
							<md-tooltip>Подтвердить изменения</md-tooltip>
						</md-button>
						<md-button class='md-icon-button' ng-click='cancelChanges()'>
							<md-icon class='warn-icon' md-svg-src="{{icons.remove}}"></md-icon>
							<md-tooltip>Отмена</md-tooltip>
						</md-button>
						{{clEdtCtrl.a}}
					</div>
			</div>
		`;

		angular.element(elem).append(ctrl.$compile(appendString)(scope))
	}
};

 require("./styles.scss");

function Controller($scope, $compile) {
	const vm = this;
	vm.$compile = $compile
	vm.cellOptions = $scope.cellOptions;

	vm.editMode = false;

	vm.oldValue = _.get($scope.cellData.point, "value", null);

	$scope.cancelChanges = function(){
		vm.editMode = false;
		if(!_.isNull(vm.oldValue)){
			$scope.cellData.point.value = vm.oldValue;
		}
	}

	$scope.changeEditMode = function(mode){
		vm.editMode = mode;
	}

	$scope.saveChanges = function(cellData){
		$scope.cellOptions[vm.editMode].onClick(cellData);
		vm.editMode = false;
	}

	$scope.icons = {
		remove: require("images/icons/ic_clear_black_24px.svg"),
		edit: require("images/icons/ic_mode_edit_black_24px.svg"),
		confirm: require("images/icons/ic_done_black_24px.svg")
	}
}

angular.module('app.home',[])
	.config(route);

function requireAll(r) {
	r.keys().forEach(r);
}
requireAll(require.context("./", true, /\.js$/));

function route($stateProvider) {
	$stateProvider.state({
		name: 'app',
		url: '/app',
		template: require("./index.html"),
		onEnter: function($state, $rootScope){
			if(!$rootScope.isAuth){
				$state.transitionTo('auth.signIn')
			}
		}
	})
}
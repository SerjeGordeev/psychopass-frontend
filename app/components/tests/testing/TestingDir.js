angular.module('app.tests')
	.directive('testing', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./Testing.html"),
	controller: Controller,
	controllerAs: "tstgCtrl",
	scope: {}
}

function Controller($q, $state, $scope, Users, Auth,
 flashAlert, Tests, Properties, $mdDialog, Moment) {
	const vm = this;

	vm.test = null;
	vm.user = null;

	vm.completeTest = completeTest;

	init();
	function init(){
		vm.promise = $q.all({
			test: Tests.getOne($state.params.id),
			user: Users.api().getList({_id: Auth.currentUser._id})
		}).then(response=>{
			vm.test = response.test;
			vm.user = response.user[0];
		});
	}

	function completeTest(results, ev){
		let result = Tests.getResults(results, vm.test);
		//console.log( vm.user.properties)
		let propData = _.find(vm.user.properties, {_id: vm.test.propertyData._id});

		if(!propData){
			propData = {_id: vm.test.propertyData._id, data: []}
			vm.user.properties.push(propData)
		}

		let actuallVal = _.find(propData.data, {actually: true});

		if (actuallVal && actuallVal.value > 0) {
			actuallVal.value = (actuallVal.value+result.propValue)/2;
		} else {
			propData.data.push({
				value: result.propValue,
				actually: true,
				date: Moment.moment().format("DD.MM.YYYY"),
				by: {
					type: "test",
					subjectId: vm.test.id
				}
			});
		}

		console.log(actuallVal, propData)
		vm.user.put().then(()=>{
			//сохранить отметку о прохождении теста
			vm.test.passings.push({
				memberId: vm.user.id,
				date: Moment.moment().format("DD.MM.YYYY"),
				answers: results,
				result: result.propValue
			});
			return vm.test.put()
		}).then(()=>{
			showResults(result, ev);
		}).catch(err=>{
			console.log(err)
		});
	}

	function showResults(results, ev){

	    $mdDialog.show({
	      controller: DialogController,
	      template: require("./TestResultDialog.html"),
	      parent: angular.element(document.body),
	      targetEvent: ev,
	      clickOutsideToClose: true
	    })
	    .then(()=>{
	    	$state.transitionTo("app.tests");
	    });
		
		function DialogController($scope, $mdDialog) {

			$scope.results = results;

		    $scope.hide = function() {
		      $mdDialog.hide();
		    };

		    $scope.cancel = function() {
		      $mdDialog.cancel();
		    };

		    $scope.answer = function() {
		      $mdDialog.hide();
		    };
		}
	}
}

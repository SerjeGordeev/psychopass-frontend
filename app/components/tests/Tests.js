angular.module('app.tests')
	.directive('tests', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./Tests.html"),
	controller: Controller,
	controllerAs: "tstsCtrl",
	scope: {}
}

function Controller($q, $state, $scope, Users, Auth, flashAlert, Tests, Properties, $mdDialog) {
	const vm = this;

	vm.tests = [];

	vm.testsTypes = Tests.testsTypes;

	vm.viewMode = "list";
	vm.editMode = false;
	vm.editingTest = null;

	vm.CRUD = ["admin", "psycholog"];
	
	vm.isCRUD = _.includes(vm.CRUD, Auth.currentUser.role);
	vm.isStudent = Auth.currentUser.role === "student";

	vm.addTest = addTest;
	vm.removeTest = removeTest;
	vm.updateTest = updateTest;
	vm.toogleCreateMode = toogleCreateMode;
	vm.editQuestions = editQuestions;
	vm.editTranslation = editTranslation;
	vm.getTesting = getTesting;
	vm.openPassings = openPassings;

	init();

	function init(){
		vm.promise = $q.all({
			tests: Tests.getTests({},{users: true}),
			properties: Properties.getProperties()
		})
		.then(response=>{
			vm.properties = response.properties;
			vm.tests = _.map(response.tests, (test) => {
				_.initUpdating(test);
				return test;
			});

			//vm.editingTest = vm.tests[0];
			//vm.viewMode = "editTranslation"
		});
	}

	function addTest() {
		vm.promise = Tests.api().post({
			name: " Новый тест",
			type: "own"
		}).then(()=>{
			init();
			flashAlert.success("Новая тест добавлен")
		});
	}

	function removeTest(test){
		vm.promise = test.remove().then(()=>{
			init();
			flashAlert.success(`Тест ${_.get(test, "name", "")} удалена`);
		});
	}

	function updateTest(test) {
		if(_.isUpdated(test)){
			vm.promise = test.put().then(()=>{
				init();
				flashAlert.success("Данные теста обновлены")
			});
		}
	}

	function toogleCreateMode(newTest){
		vm.viewMode = "list"
		vm.editingTest = null;
		if(newTest){
			init();
		}	
	}

	function editQuestions(test){
		vm.editingTest = test;
		vm.viewMode = "editQuestions"
	}

	function editTranslation(test){
		vm.editingTest = test;
		vm.viewMode = "editTranslation"
	}

	function getTesting(test) {
		if(test.type === 'external'){
			window.open(test.adress);
		} else {
			$state.transitionTo("app.testing",{id: test.id});
		}
	}

	function openPassings(ev, test){
		$mdDialog.show({
			controller: DialogController,
			template: require("./TestPassingsReportDialog.html"),
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose: true
		});

		function DialogController($scope, $mdDialog) {
			$scope.test = test;

			$scope.hide = function() {
				$mdDialog.hide();
			};

			$scope.cancel = function() {
				$mdDialog.cancel();
			};

			$scope.answer = function() {
				$mdDialog.hide();
			};
		}
	}
}

angular.module('app.common')
	.directive('editQuestionsForm', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./EditQuestionsForm.html"),
	controller: Controller,
	controllerAs: "edtQstCtrl",
	scope: {
		test: "=",
		callback: "="
	},
	replace: true
};

require("./styles.scss");

function Controller($scope, Properties, Tests, flashAlert, Auth) {
	const vm = this;

	vm.properties = [];

	vm.icons = {
		remove: require("images/icons/ic_clear_black_24px.svg")
	}

	vm.questionTypes = [
		{
			type: "radio",
			title: "Выбор одного варианта"
		},
		{
			type: "checkbox",
			title: "Выбор нескольких вариантов"
		},
		{
			type: "text",
			title: "Текстовое поле"
		}
	]

	vm.variantTemplate = {
		value: null,
		weight: null
	}

	vm.questionTemplate = {
		    title: null,
		    description: null,
      		type: "radio",
      		variants: []
	}

	vm.saveTest = saveTest;
	vm.addQuestion = addQuestion;
	vm.removeQuestion = removeQuestion;
	vm.addVariant = addVariant;
	vm.removeVariant = removeVariant;
	vm.togglePreviewMode = togglePreviewMode;

	init();
	function init(){
		//console.log($scope.test)
	}

	function addQuestion(){
		$scope.test.questions.push(angular.copy(vm.questionTemplate))
	}

	function addVariant(question){
		question.variants.push(angular.copy(vm.variantTemplate))
	}

	function saveTest(){
		$scope.test.put()
		.then((test)=>{
			flashAlert.success(`Тест ${$scope.test.name} успешно обновлен`);
			$scope.callback(test);
		})
		
	}

	function removeVariant(question, ix){
		question.variants.splice(ix, 1)
	}

	function removeQuestion(ix){
		$scope.test.questions.splice(ix, 1)
	}

	function togglePreviewMode(results){
		console.log(results)
		vm.previewMode = !vm.previewMode; 
	}

}

angular.module('app.common')
	.directive('editTranslationForm', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./EditTranslationForm.html"),
	controller: Controller,
	controllerAs: "edtTrslCtrl",
	scope: {
		test: "=",
		callback: "="
	},
	replace: true
};

require("./styles.scss");

function Controller($scope, flashAlert) {
	const vm = this;

	vm.icons = {
		remove: require("images/icons/ic_clear_black_24px.svg")
	};

	vm.properties = [];

	vm.rangeTemplate = {
		range: [null, null],
      	message: null,
      	propValue: null
	};

	vm.saveTest = saveTest;
	vm.addRange = addRange;
	vm.removeRange = removeRange;

	vm.getMinLimit = getMinLimit;
	vm.getMaxLimit = getMaxLimit;

	init();
	function init(){
		vm.maxValue = 0;
		_.forEach($scope.test.questions, question=>{
			_.forEach(question.variants, variant=>{
				vm.maxValue += parseInt(variant.weight, 10)
			});
		});	
	}

	function getMinLimit(range, ix){
		let prevRange = $scope.test.translation[ix-1]?$scope.test.translation[ix-1].range[1]:null;
		return prevRange?prevRange+1:0
	}

	function getMaxLimit(range, ix){
		let nextRange = $scope.test.translation[ix+1]? $scope.test.translation[ix+1].range[0]:null;
		return nextRange?nextRange-1:vm.maxValue
	}

	function saveTest(){
		$scope.test.put()
		.then((test)=>{
			flashAlert.success(`Тест ${$scope.test.name} успешно обновлен`);
			$scope.callback(test);
		})
		
	}

	function addRange(){
		$scope.test.translation.push(angular.copy(vm.rangeTemplate))
	}

	function removeRange(ix){
		$scope.test.translation.splice(ix, 1)
	}


}

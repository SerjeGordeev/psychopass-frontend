angular.module('app.tests',[])
	.config(route);

function requireAll(r) {
	r.keys().forEach(r);
}
requireAll(require.context("./", true, /\.js$/));

require("./styles.scss");

function route($stateProvider) {
	$stateProvider.state({
		name: 'app.tests',
		url: '/tests',
		template: "<tests></tests>"
	})

	$stateProvider.state({
		name: 'app.testing',
		url: '/tests/:id',
		template: "<testing></testing>"
	})
}
angular.module('app.common')
	.directive('testView', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./TestView.html"),
	controller: Controller,
	controllerAs: "tstVwCtrl",
	scope: {
		test: "=?",
		callback: "=?"
	},
	replace: true
};

require("./styles.scss");

function Controller($scope, $state, Properties, Tests, flashAlert, Auth) {
	const vm = this;

	vm.answersCount = 0;
	vm.completeTest = completeTest;

	$scope.$watch(()=>$scope.test.questions, (nV)=>{
		vm.answersCount = _.filter(nV, question=>{
			return !_.isUndefined(question.answer)
		}).length
	}, true);

	function completeTest(ev){
		
		let results = _.map($scope.test.questions, "answer");
		$scope.callback(results, ev);
	}
}

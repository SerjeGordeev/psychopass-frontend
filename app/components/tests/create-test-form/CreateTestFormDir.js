angular.module('app.common')
	.directive('createTestForm', () => DIR_PARAMS);

var DIR_PARAMS = {
	template: require("./CreateTestForm.html"),
	controller: Controller,
	controllerAs: "crtTstCtrl",
	scope: {
		// isExternal: "=",
		// createTestData: "="
		callback: "="
	},
	replace: true
};

require("./styles.scss");

function Controller($scope, Properties, Tests, flashAlert, Auth) {
	const vm = this;

	vm.properties = [];

	vm.testsTypes = Tests.testsTypes;
	console.log(vm.testsTypes)

	vm.formData = {
		name: null,
		description: null,
		propId: null,
		type: "own",
		authorId: Auth.currentUser._id
	}

	vm.createTest = createTest;

	init();
	function init(){
		vm.promise = Properties.getProperties().then(props=>{
			vm.properties = props;
		});
	}

	function createTest(){
		vm.promise = Tests.api().post(vm.formData)
		.then((newTest)=>{
			flashAlert.success(`Тест ${vm.formData.name} успешно создан`)
			$scope.callback(newTest);
		})
		
	}


}
